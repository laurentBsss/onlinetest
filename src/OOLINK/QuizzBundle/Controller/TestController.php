<?php

namespace OOLINK\QuizzBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class TestController extends Controller
{
    public function sendTestAction(Request $request)
    { 
        $defaultData = array('QCM' => 'PHP');
  		$form = $this->createFormBuilder($defaultData)
        ->add('email', EmailType::class)
        //->add('message', TextareaType::class)
        ->add('envoyer', SubmitType::class)
        ->getForm();

	    $form->handleRequest($request);

	    if ($form->isSubmitted() && $form->isValid()) {
	        // data is an array with "name", "email", and "message" keys
	        $data = $form->getData();
	        //var_dump($data);die;
	        $crypt= "";
	        

	        $message = \Swift_Message::newInstance()
		        ->setSubject('QCM test :'.$defaultData['QCM'] )
		        ->setFrom('info@brainquizz.com')
		        ->setTo(''.$data['email'])
		        //->setTo('tech@oolink.fr') 
		        ->setBody(
		            "<html>http://quizzoolink.fr/app_dev.php/quizz/</html>",
		            'text/html'
		        );

		    $this->get('mailer')->send($message);

		   /* var_dump($message);die;*/
		   return new Response("mail send  ok");
	    }




        return $this->render('OOLINKQuizzBundle:Dashboard/Test:send.html.twig', array(
      'form' => $form->createView(), ) );
    }

    public function historiqueAction()
    {   
    	var_dump("historique");die;
        return $this->render('OOLINKQuizzBundle:Dashboard:main.html.twig');
    }
}
