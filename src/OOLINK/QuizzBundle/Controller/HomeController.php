<?php

namespace OOLINK\QuizzBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    public function indexAction()
    {
        return $this->render('OOLINKQuizzBundle:Home:home.html.twig');
    }

    public function testMailAction()
    {  //var_dump(phpinfo());die;
        $message = \Swift_Message::newInstance()
        ->setSubject('Hello Email')
        ->setFrom('info@brainquizz.com')
        ->setTo('tech@oolink.fr')
        ->setBody(
            "<html>test herererererere</html>",
            'text/html'
        ) ;

        $logger = new \Swift_Plugins_Loggers_ArrayLogger;
		//$logger = new \Swift_Plugins_Loggers_EchoLogger; //echo messages in real-time
		//$message->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));
       
	    $this->get('mailer')->send($message);
	    var_dump( $logger->dump());

	    var_dump('</br>');
	    //printf("Sent %d messages\n", );
	    var_dump($logger);

        return new Response("mail send  ok");
    }

     public function createAction()
    {
        return $this->render('OOLINKQuizzBundle:Home:create.html.twig');
    }

    public function entrepriseAction()
    {
        return $this->render('OOLINKQuizzBundle:Entreprise:index.html.twig');
    }

    public function testeurAction()
    {
        return $this->render('OOLINKQuizzBundle:Testeur:testeur.html.twig');
    }
}
