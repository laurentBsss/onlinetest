<?php

namespace OOLINK\QuizzBundle\Controller;

use OOLINK\QuizzBundle\Entity\Entreprise;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('OOLINKQuizzBundle:Default:index.html.twig');
    }

    public function addEntrepriseAction(Request $request)
    {
   		 $entreprise = new Entreprise();
   		 $entreprise->setCreatedAt(new \Datetime());

	    $form = $this->get('form.factory')->createBuilder(FormType::class, $entreprise)
	      //->add('createdAt',      DateTimeType::class)
	      ->add('nom',     TextType::class)
	      ->add('email',     EmailType::class)
	      ->add('credits',   IntegerType::class)
	      
	      ->add('Send',      SubmitType::class)
	      ->getForm()

	    ;

	    // Si la requête est en POST

	    if ($request->isMethod('POST')) {

	      $form->handleRequest($request);

	      if ($form->isValid()) {

	        $em = $this->getDoctrine()->getManager();
	        $em->persist($entreprise);
	        $em->flush();

	       // $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

	        return $this->redirectToRoute('oolink_quizz_homepage', array('id' => $entreprise->getId()));

	      }

	    }


    return $this->render('OOLINKQuizzBundle:Default:index.html.twig', array(
      'form' => $form->createView(), ));

    

  }
        
}
