<?php

namespace OOLINK\QuizzBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{
    public function indexAction()
    {
        return $this->render('OOLINKQuizzBundle:Dashboard:main.html.twig');
    }
}
