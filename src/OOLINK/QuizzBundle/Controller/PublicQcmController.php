<?php

namespace OOLINK\QuizzBundle\Controller;

use OOLINK\QuizzBundle\Entity\Entreprise;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class PublicQcmController extends Controller
{
    public function indexAction()
    {
        return $this->render('OOLINKQuizzBundle:QCM:index.html.twig');
    }

    
        
}
