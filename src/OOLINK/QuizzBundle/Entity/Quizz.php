<?php

namespace OOLINK\QuizzBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Quizz
 *
 * @ORM\Table(name="quizz")
 * @ORM\Entity(repositoryClass="OOLINK\QuizzBundle\Repository\QuizzRepository")
 */
class Quizz
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="techno_id", type="integer", unique=true)
     */
    private $technoId;

    /**
     * @var string
     *
     * @ORM\Column(name="niveau", type="string", length=100)
     */
    private $niveau;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=255)
     */
    private $question;

    /**
     * @var string
     *
     * @ORM\Column(name="reponse", type="string", length=255)
     */
    private $reponse;

    /**
     * @var string
     *
     * @ORM\Column(name="reponse_bonne", type="string", length=255)
     */
    private $reponseBonne;

    /**
     * @var string
     *
     * @ORM\Column(name="langue", type="string", length=10)
     */
    private $langue;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set technoId
     *
     * @param integer $technoId
     *
     * @return Quizz
     */
    public function setTechnoId($technoId)
    {
        $this->technoId = $technoId;

        return $this;
    }

    /**
     * Get technoId
     *
     * @return int
     */
    public function getTechnoId()
    {
        return $this->technoId;
    }

    /**
     * Set niveau
     *
     * @param string $niveau
     *
     * @return Quizz
     */
    public function setNiveau($niveau)
    {
        $this->niveau = $niveau;

        return $this;
    }

    /**
     * Get niveau
     *
     * @return string
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return Quizz
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set reponse
     *
     * @param string $reponse
     *
     * @return Quizz
     */
    public function setReponse($reponse)
    {
        $this->reponse = $reponse;

        return $this;
    }

    /**
     * Get reponse
     *
     * @return string
     */
    public function getReponse()
    {
        return $this->reponse;
    }

    /**
     * Set reponseBonne
     *
     * @param string $reponseBonne
     *
     * @return Quizz
     */
    public function setReponseBonne($reponseBonne)
    {
        $this->reponseBonne = $reponseBonne;

        return $this;
    }

    /**
     * Get reponseBonne
     *
     * @return string
     */
    public function getReponseBonne()
    {
        return $this->reponseBonne;
    }

    /**
     * Set langue
     *
     * @param string $langue
     *
     * @return Quizz
     */
    public function setLangue($langue)
    {
        $this->langue = $langue;

        return $this;
    }

    /**
     * Get langue
     *
     * @return string
     */
    public function getLangue()
    {
        return $this->langue;
    }
}

