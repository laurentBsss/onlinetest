<?php

namespace OOLINK\QuizzBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resultats
 *
 * @ORM\Table(name="resultats")
 * @ORM\Entity(repositoryClass="OOLINK\QuizzBundle\Repository\ResultatsRepository")
 */
class Resultats
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="techno_id", type="integer")
     */
    private $technoId;

    /**
     * @var string
     *
     * @ORM\Column(name="niveau", type="string", length=255)
     */
    private $niveau;

    /**
     * @var int
     *
     * @ORM\Column(name="score", type="integer")
     */
    private $score;

    /**
     * @var int
     *
     * @ORM\Column(name="candidat_id", type="integer")
     */
    private $candidatId;

    /**
     * @var int
     *
     * @ORM\Column(name="client_id", type="integer")
     */
    private $clientId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Resultats
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set technoId
     *
     * @param integer $technoId
     *
     * @return Resultats
     */
    public function setTechnoId($technoId)
    {
        $this->technoId = $technoId;

        return $this;
    }

    /**
     * Get technoId
     *
     * @return int
     */
    public function getTechnoId()
    {
        return $this->technoId;
    }

    /**
     * Set niveau
     *
     * @param string $niveau
     *
     * @return Resultats
     */
    public function setNiveau($niveau)
    {
        $this->niveau = $niveau;

        return $this;
    }

    /**
     * Get niveau
     *
     * @return string
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return Resultats
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return int
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set candidatId
     *
     * @param integer $candidatId
     *
     * @return Resultats
     */
    public function setCandidatId($candidatId)
    {
        $this->candidatId = $candidatId;

        return $this;
    }

    /**
     * Get candidatId
     *
     * @return int
     */
    public function getCandidatId()
    {
        return $this->candidatId;
    }

    /**
     * Set clientId
     *
     * @param integer $clientId
     *
     * @return Resultats
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get clientId
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }
}

