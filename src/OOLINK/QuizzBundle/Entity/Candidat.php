<?php

namespace OOLINK\QuizzBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Candidat
 *
 * @ORM\Table(name="candidat")
 * @ORM\Entity(repositoryClass="OOLINK\QuizzBundle\Repository\CandidatRepository")
 */
class Candidat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="resultat_id", type="integer", nullable=true, unique=true)
     */
    private $resultatId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set resultatId
     *
     * @param integer $resultatId
     *
     * @return Candidat
     */
    public function setResultatId($resultatId)
    {
        $this->resultatId = $resultatId;

        return $this;
    }

    /**
     * Get resultatId
     *
     * @return int
     */
    public function getResultatId()
    {
        return $this->resultatId;
    }
}

