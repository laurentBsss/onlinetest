<?php

namespace OOLINK\QuizzBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Quizz_temp
 *
 * @ORM\Table(name="quizz_temp")
 * @ORM\Entity(repositoryClass="OOLINK\QuizzBundle\Repository\Quizz_tempRepository")
 */
class Quizz_temp
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="qcm_final", type="string", length=255)
     */
    private $qcmFinal;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255)
     */
    private $token;

    /**
     * @var int
     *
     * @ORM\Column(name="candidat_id", type="integer")
     */
    private $candidatId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qcmFinal
     *
     * @param string $qcmFinal
     *
     * @return Quizz_temp
     */
    public function setQcmFinal($qcmFinal)
    {
        $this->qcmFinal = $qcmFinal;

        return $this;
    }

    /**
     * Get qcmFinal
     *
     * @return string
     */
    public function getQcmFinal()
    {
        return $this->qcmFinal;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Quizz_temp
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set candidatId
     *
     * @param integer $candidatId
     *
     * @return Quizz_temp
     */
    public function setCandidatId($candidatId)
    {
        $this->candidatId = $candidatId;

        return $this;
    }

    /**
     * Get candidatId
     *
     * @return int
     */
    public function getCandidatId()
    {
        return $this->candidatId;
    }
}

